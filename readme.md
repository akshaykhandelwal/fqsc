### First Quality Sitecore Repository
__Sitecore Version:__ 9.0

## Configuration Management
All configuration changes to Sitecore.Config will be implemented using Sitecore
patch configs.  

Default versions of global XML configuration files such as Domain.config,
Sites.config, ConnectionStrings.config, etc., will live in the 
Website\App\_Config\Includes folder, with the extension .config.original, and
our custom versions of these files will live next to them with the extension
.config, and we will use xinclude and xpointer from XML 1.1 standard to include
the portions of these original configurations that we need.

Any configurations specific to a module or function will live in a folder named
Config inside the module's folder. The MS Build process will copy the files
into place. 

