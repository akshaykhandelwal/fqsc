# Required Files
## Purpose
The build process requires files that should not be present in the repository.
Some files will need to be manually added here, others will be dynamically
added by the build process.

## Restrictions
Never add files to the repository in this folder.

## Manual Files Required
License.xml file must be present here for local builds to succeed.
