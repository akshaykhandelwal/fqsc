# Scripts

## Purpose
Utility scripts for builds and deployment.

## Restrictions
Only build and deploy scripts should be present here. Any code that is actually used by the site should live in the Foundation, Feature, or Project layers. Code present here should only facilitate builds and deployment of the built files.

See https://github.com/Sitecore/Habitat/tree/master/scripts for examples of what type of scripts generally go here.
