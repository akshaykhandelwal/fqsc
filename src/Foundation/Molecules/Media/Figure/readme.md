### Description
A figure containing and image and a caption

### Variables
|Name|Pattern  |
|--|--|
| Image | <ul><li>Image</li>OR<li>Picture</li></ul> |
| Caption | ```figcaption``` |
