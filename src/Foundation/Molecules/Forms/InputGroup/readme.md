
### Description
Two or more radio or checkbox inputs that are assigned the same name and function as a group, wrapped inside a container

### Variables
Name | Pattern
------------ | -------------
Input | RadioInput or CheckboxInput
