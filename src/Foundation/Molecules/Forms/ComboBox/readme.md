### Description
A select list with input box. May be implemented with HTML5 datalist in
supported browsers, but needs cross browser solution.

### Variables
|Name|Pattern |
|--|--|
| TextInput | TextInput |
| Select | SelectInput |
