### Description
A search input and button, typically used in header or footer

### Variables
|Name|Pattern |
|--|--|
| Search | Input(type=search) |
| Submit | Button(type=submit) |
