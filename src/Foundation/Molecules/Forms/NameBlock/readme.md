### Description
A set of form fields for filling out a name

### Variables
Name | Pattern
------------ | -------------
Honorific | ComboBox
First  | TextInput
Middle  | TextInput
Last  | TextInput
Suffix | TextInput
