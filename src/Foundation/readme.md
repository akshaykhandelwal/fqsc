# Foundation Layer Source Files

## Purpose
Contains all front end and backend code for the Foundation Layer.

This layer forms the foundation of the solution. When a change occurs in one of these modules it can impact many other modules in the solution. The Foundation layer should be the most stable layer in terms of the Stable Dependencies Principle: Depend in the direction of Stability.

This project will follow a model of dependency injection. For example, if a Feature module needs to be indexable by Search, then a Service should be defined that allows the individual Feature module to register itself as being indexable. The Search Service doesn't need to know about specific modules, but is able to discover all modules that support being indexed. For deeper understanding, see the Habitat project example's methods for handling indexing.

Greater abstraction leads to greater stability. Ensure that the closer to the top of the dependency tree a module is, the more abstract the code becomes. Focus on using interfaces instead of concrete classes. Focus on dependency injection instead of explicit declarations. Focus on abstract functionality instead of concrete implementation.

## Restrictions
No files should be added in the root. All files should be added to module folders.

If a Module in this layer must depend on other modules in this layer, the dependency must abide by the Acyclic Dependencies Principle: The dependency graph of packages must have no cycles. In short, if Module X depends on Module A, Module A cannot depend on Module X, nor can it depend on any other module that depends on Module X. Dependency must be unidirectional with no cycles.

Unless there is an extremely solid business case to do otherwise, modules in the Foundation layer must be conceptually abstract and not contain presentation in the form of renderings or views (as renderings and views are considered concrete). In order to control dependencies, any presentation related functionality at the Foundation layer should have all Feature or Project specific knowledge passed as parameters from the depending module.

In terms of Atomic Design for front end, this generally means the Foundation layer likely will contain atoms and molecules, but will not contain organisms, templates, pages, or any json data for custom templates or testing. Organisms will live at the Feature layer. Test data will live at the Project layer.

In terms of Helix for front end and back end, the Foundation layer will not contain presentation. The Feature will define the concrete implementation. The Project layer will be used to define the custom look/feel for each brand.

