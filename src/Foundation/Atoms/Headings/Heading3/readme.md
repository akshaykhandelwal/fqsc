### Description
An H3 element

### Configurable Properties
+ **class** (String? Array?)
: A list of classes to be applied to the atom
+ **content** (integer)
: The content of the heading
