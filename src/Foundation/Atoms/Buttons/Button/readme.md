### Description
A generic button

### Configurable Properties
+ **type** (String)
: Supported values: button, submit
+ **class** (String)
: A list of classes to be applied to the atom
+ **label** (String)
: Display label
