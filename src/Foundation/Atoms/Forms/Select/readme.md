### Description
A select element with options

### Configurable Properties
+ **class** (String? Array?)
: A list of classes to be applied to the atom
+ **name** (String)
: Used on form elements to submit information
+ **id** (String)
: Used to link atom to a label only if not contained in a label element
+ **required** (Boolean)
: Determines if the atom is required for the form
+ **options** (Array of Objects)
: A list of options consisting of text and value pairs
+ **label** (String)
: Label content
