### Description
A textarea form field

### Configurable Properties
+ **class** (String? Array?)
: A list of classes to be applied to the atom
+ **name** (String)
: Used on form elements to submit information
+ **id** (String)
: Used to link atom to a label only if not contained in a label element
+ **required** (Boolean)
: Determines if the atom is required for the form
+ **label** (String)
: Label content
