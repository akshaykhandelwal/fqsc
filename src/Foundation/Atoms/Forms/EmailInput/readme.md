### Description
An email input

### Configurable Properties
+ **type** (String)
: A valid HTML input type, as defined [here](https://www.w3schools.com/tags/tag_input.asp)
+ **class** (String? Array?)
: A list of classes to be applied to the atom
+ **name** (String)
: Used on form elements to submit information
+ **id** (String)
: Used to link atom to a label only if not contained in a label element
+ **required** (Boolean)
: Determines if the atom is required for the form
+ **placeholder** (String)
: A string that is pre-filled as the value
+ **value** (String)
: A prepopulated value for the input
+ **label** (String)
: Label content
