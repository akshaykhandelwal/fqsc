### Description
A generic paragraph element

### Configurable Properties
+ **class**  (String)
: A list of classes to be applied to the atom
