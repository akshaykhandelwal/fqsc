### Description
A details block with optional summary element

### Configurable Properties
+ **class** (String? Array?)
: A list of classes to be applied to the atom
+ **summary** (String)
: The heading of the detail element
+ **content** (String)
: The content of the details block
