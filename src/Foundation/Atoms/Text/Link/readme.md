### Description
A link element

### Configurable Properties
+ **class** (String)
: A list of classes to be applied to the atom
+ **href** (String)
: A valid url
+ **target** (String)
: Specifies where to open the linked document
+ **text** (String)
: The link text
