### Description
A generic block quote with citation and quote

### Configurable Properties
+ **class** (String? Array?)
: A list of classes to be applied to the atom
+ **quote** (String)
: The content of the quote
+ **citation** (String)
: An author or citation
