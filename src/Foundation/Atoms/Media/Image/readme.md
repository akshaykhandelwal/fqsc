### Description
An image with optional sourceset

### Configurable Properties
+ **class** (String? Array?)
: A list of classes to be applied to the atom
+ **src** (String)
: A valid image address.
+ **srcset** (String? Array? Object?)
: A list of one or more strings separated by commas indicating a set of possible image sources for the user agent to use.
+ **alt** (String)
: A description of the image. Ideal for accessibility and SEO.
