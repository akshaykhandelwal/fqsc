# Feature Layer Source Files

## Purpose
Contains all front end and backend code for features.

## Restrictions
No files may be added to the root folder. All Features must be contained in
their individual folders.

Features depend on Foundation, and no feature depends on another feature.

Classes that change together are packaged together.

See [Helix Feature Layer](http://helix.sitecore.net/principles/architecture-principles/layers.html#feature-layer) for more details.
