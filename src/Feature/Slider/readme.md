# Slider Source and Data Files

## Purpose
The business needs a way to render multiple pieces of content in a manner that can be consolidated into a single interface generally called a "rotator" or "slider" or "carousel". Since any changes to one type of slider would impact another type of slider, any slider type, such as the generic slider, the hotspot slider, etc., should be a part of this feature module.

## Implementation
All sliders should support the default concept of embedding slides, providing arrows and/or dots, providing automatic rotation animation, and inherits from IContainer, which automatically registers the rendering with the ContainerService, allowing any children renderings that implement IContainerHandler to add themselves to the Container's list of handlers, manipulating the container's data before the container renders. The ContainerService behaves like a stack, with the IContainer object being added to the stack, and popped from the stack after the handlers are all executed. IContainerMultiple executes all descendant handlers, even those that are executed for nested containers. IContainerSingle only executes descendant handlers that are not defined inside a nested container. 

## Restrictions
No business specific logic here, only shared standard behaviors and a base design.

This feature cannot depend on other features. Slides inside a slider need to be independent of this feature's code. This can be achieved using a dynamic placeholder, a datasoure to drive the children slides, or other methods that do not directly hardcode a slide item in this feature. Sliders should not care what their slide content is, nor be tied to that content.

No files should be added at the root folder. All synchronization files should be stored in the data folder, and all code files should be stored in the src folder.
